module Imp = ImpAST
module Gto = GotoAST
open CommonAST

let (++) = Gto.(++)

let rec translate_expression = function
  | Imp.Literal lit ->
    Gto.Literal lit

  | Imp.Location loc ->
    Gto.Location(translate_location loc)

  | Imp.UnaryOp(op, e) ->
    Gto.UnaryOp(op, translate_expression e)

  | Imp.BinaryOp(op, e1, e2) ->
    Gto.BinaryOp(op, translate_expression e1, translate_expression e2)

  (* Création d'un bloc : rien de spécial. *)
  | Imp.NewBlock(e) ->
    Gto.NewBlock(translate_expression e)
      
  | Imp.FunCall(id, e_list) -> let gto_list = nouvelle e_list [] in Gto.FunCall(id, gto_list)


and nouvelle liste acc= 
    match liste with
    |[] -> List.rev acc
    |a::b -> let tempo = translate_expression a in let acc = tempo::acc in nouvelle b acc 

and translate_location = function
  | Imp.Identifier id ->
    Gto.Identifier id

  (* Accès à un bloc : rien de spécial. *)
  | Imp.BlockAccess(e1, e2) ->
    Gto.BlockAccess(translate_expression e1, translate_expression e2)


let new_label =
  let cpt = ref 0 in
  fun () -> incr cpt; CommonAST.Lab (Printf.sprintf "_label_%i" !cpt)

let rec translate_instruction = function
  | Imp.Sequence(i1, i2) ->
    Gto.Sequence(translate_instruction i1,
                 translate_instruction i2)
      
  | Imp.Print(e) ->
    Gto.Print(translate_expression e)
      
  | Imp.Set(loc, e) ->
    Gto.Set(translate_location loc, translate_expression e)

  | Imp.Conditional(c, i1, i2) ->
    let then_label = new_label()
    and end_label = new_label()
    in
    Gto.ConditionalGoto(then_label, translate_expression c)
    ++ translate_instruction i2
    ++ Gto.Goto end_label
    ++ Gto.Label then_label
    ++ translate_instruction i1
    ++ Gto.Label end_label

  | Imp.Loop(c, i) ->
    let test_label = new_label()
    and code_label = new_label()
    in
    Gto.Goto test_label
    ++ Gto.Label code_label
    ++ translate_instruction i
    ++ Gto.Label test_label
    ++ Gto.ConditionalGoto(code_label, translate_expression c)

  | Imp.Return(e) -> Gto.Return(translate_expression e)

  | Imp.Nop ->
    Gto.Nop

let strip_signature imp_sign = 
	let retour = Imp.(imp_sign.return) in let form = Imp.(imp_sign.formals)  in   
	{return = retour; formals = form}

let rec strip_function_info fun_info =  
        let fun_code_instruction = translate_instruction (Imp.(fun_info.code)) in
        {Gto.signature = strip_signature Imp.(fun_info.signature) ; Gto.code = fun_code_instruction}
(*cette fonction permet de passer du champs functions de SourceLocalised au champs functions Imp*)
let rec all_strip_function_info imp_functions_liste gto_functions_map=
	match imp_functions_liste with
        |[] -> gto_functions_map
        |(id_fun, imp_fun_info)::reste -> let gto_fun_info = strip_function_info imp_fun_info in
                                              let tempo_gto_function_map = Symb_Tbl.add id_fun gto_fun_info gto_functions_map in 
                                              all_strip_function_info reste tempo_gto_function_map

let translate_program p = 
  let map_vide = Symb_Tbl.empty in 
  let src_functions_liste = Symb_Tbl.bindings Imp.(p.functions) in
    let functions = all_strip_function_info src_functions_liste map_vide in
  Gto.({
  main = translate_instruction Imp.(p.main);
  globals = Imp.(p.globals);
  functions;
})
