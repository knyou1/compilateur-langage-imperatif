open CommonAST
open SourceLocalisedAST

exception Type_error of typ * typ * (int * int)
exception Array_type_expected of typ * (int * int)
exception Struct_type_expected of typ * (int * int)

let rec check_type context e expected_type =
  let e_type = type_expression context e in
  if e_type = expected_type
  then ()
  else raise (Type_error(e_type, expected_type, e.e_pos))

and type_expression context e = match e.expr with
  | Literal lit -> type_literal lit
  | Location loc -> type_location context loc 

  | UnaryOp(Minus, e) -> check_type context e TypInt; TypInt
  | UnaryOp(Not, e) -> check_type context e TypBool; TypBool
                         
  | BinaryOp(op, e1, e2) ->
    let operand_type, result_type = match op with
      | Add | Sub | Mult | Div | Mod -> TypInt, TypInt
      | Lt | Le | Gt | Ge -> TypInt, TypBool
      | And | Or -> TypBool, TypBool
      | Eq | Neq -> type_expression context e1, TypBool
    in
    check_type context e1 operand_type;
    check_type context e2 operand_type;
    result_type

  (* Typage de la création d'un tableau : l'expression donnant la taille doit
     être un entier. Le résultat est un type de tableau dont les éléments sont
     du type [ty] donné en paramètre. *)
  | NewArray(e, ty) ->
    check_type context e TypInt;
    TypArray(ty)

  (* Typage de la création d'une structure : on vérifie seulement que le nom
     existe dans le contexte de typage. *)
  | NewRecord(name) ->
    if Symb_Tbl.mem name context.struct_types
    then TypStruct(name)
    else failwith "Unknown struct"


  | FunCall(Id(id), le_list) -> 
                            let tempo = Symb_Tbl.find id (context.function_types) in 
                            let liste_arg = tempo.signature.formals in
                            let typ_function = tempo.signature.return in
                            if (check_arg le_list liste_arg context) then  typ_function
                            else failwith "mauvais argument(s)" 


and check_arg le_list liste_arg context=
    match (le_list,liste_arg) with
    |([],[]) -> true
    |(a::y,(b,c)::z) -> let typ_a = type_expression context a in 
                        if typ_a = c then check_arg y z context else false 
    |_-> false
and type_literal = function
  | Int _ -> TypInt
  | Bool _ -> TypBool

and type_location context = function
  | Identifier(Id id) -> Symb_Tbl.find id context.identifier_types

  (* Typage de l'accès à un tableau. *)
  | ArrayAccess(e1, e2) ->
    (* Calcul du type de l'expression [e1] désignant le tableau. *)
    let e1_type = type_expression context e1 in
    (* Vérification de la forme du type de [e1] *)
    let contents_type = match e1_type with
      (* S'il s'agit d'un type tableau tout va bien. On récupère le type des
         éléments du tableau. *)
      | TypArray t -> t
      (* Sinon erreur. *)
      | _ -> raise (Array_type_expected(e1_type, e1.e_pos))
    in
    (* Vérification du type de l'expression [e2] désignant l'indice entier. *)
    check_type context e2 TypInt;
    (* Résultat : type du contenu du tableau. *)
    contents_type

  (* Typage de l'accès à une structure. *)
  | FieldAccess(e, field_name) ->
    (* Calcul du type de l'expression [e] désignant la structure. *)
    let e_type = type_expression context e in
    (* Vérification de la forme du type de [e]. *)
    let fields = match e_type with
      (* S'il s'agit d'un type de structure tout va bien. On récupère la liste
         des champs et de leurs types. *)
      | TypStruct(name) -> (Symb_Tbl.find name context.struct_types).fields
      (* Sinon erreur. *)
      | _ -> raise (Struct_type_expected(e_type, e.e_pos))
    in
    (* Résultat : type du champ dont le nom est [field_name]. 
       Note : cette expression déclenchera l'exception [Not_found] si le champ
       n'existe pas. On pourrait utiliser en plus un [try/with] pour renvoyer
       dans ce cas un diagnostic plus précis. *)
    List.assoc field_name fields

   
let rec typecheck_instruction context i = match i.instr with
  | Print e -> check_type context e TypInt

  | Set(loc, e) ->
    let loc_type = type_location context loc in
    check_type context e loc_type

  | Conditional(e, i1, i2) ->
    check_type context e TypBool;
    typecheck_instruction context i1;
    typecheck_instruction context i2

  | Loop(e, i) ->
    check_type context e TypBool;
    typecheck_instruction context i
    
  | Sequence(i1, i2) ->
    typecheck_instruction context i1;
    typecheck_instruction context i2

  | Return(e) -> 
    let _ = type_expression context e in ()

  | Nop -> ()

(*vérifie que une fonction a le type de retour attendu*)

let rec check_pas_de_return instru = 
	match instru.instr with
      |Sequence(i1, i2) -> (check_pas_de_return i1) && (check_pas_de_return i2)   
      |Conditional(e,i1,i2) -> (check_pas_de_return i1) && (check_pas_de_return i2)   
      |Loop(e,i) -> check_pas_de_return i
      |Return(e) -> failwith "pas de return dans le main" 
      |_-> true
  

let rec check_good_return type_attendu instru context=
      match instru.instr with
      |Conditional(e,i1,i2) -> (check_good_return type_attendu i1 context) && (check_good_return type_attendu i2 context)  
      |Loop(e,i) -> check_good_return type_attendu i context
      |Sequence(i1, i2) -> (check_good_return type_attendu i1 context) && (check_good_return type_attendu i2 context)  
      |Return(e) -> let type_retour = type_expression context e in type_retour = type_attendu  
      |_-> true

(*ajoute au contexte les arguments de la fonction déclarée*)

let rec context_fonction_decl context_id_ty liste_arg_fonction =
	match liste_arg_fonction with
        |[] -> context_id_ty
        |(id,typ_arg)::reste -> let context_id_ty = Symb_Tbl.add id typ_arg (context_id_ty) in context_fonction_decl context_id_ty reste

(*vérifie que la déclaration d'une fonction est bien typée*)

let rec typecheck_decl_function f_info context=  
       
       (let new_context_f_id_types = context_fonction_decl (context.identifier_types) (f_info.signature.formals) in 
       let new_context_f = {identifier_types = new_context_f_id_types; 
                            struct_types= context.struct_types;
                            function_types = context.function_types } in
       (if (check_good_return (f_info.signature.return) (f_info.code) (new_context_f)) then 
        typecheck_instruction new_context_f (f_info.code) 
       else failwith "type de retour de fonction déclarée pas bon"))
       

let rec typecheck_all_decl_function liste_fonction context = 
	match liste_fonction with
        |[] -> ()
        |(id_f,f_info)::reste -> typecheck_decl_function f_info context ; typecheck_all_decl_function reste context  



(*utilier les fonctions déjà défini*)


(*comme il y a 2 types function_info: 1 défini das CommonAST, l'autre dans SourceLocalisedAST, 
on utilise cette fonction pour passer du 2ème au 1er.*)
let rec function_info_Src_To_function_info_Common map new_map=
	if Symb_Tbl.is_empty map then new_map
        else let (element_id, f_info) = Symb_Tbl.choose map in 
             let new_map = Symb_Tbl.add element_id ({signature = f_info.signature}) new_map in
             let map = Symb_Tbl.remove element_id map in
             function_info_Src_To_function_info_Common  map new_map
                               

let extract_context p =
  { identifier_types = p.globals;
    (* Le contexte de typage contient maintenant en plus la table des 
       structures. *)
    struct_types = p.structs; 
    function_types = let map_fun_common = Symb_Tbl.empty in function_info_Src_To_function_info_Common p.functions map_fun_common;
}

let typecheck_program p =
  let _ = check_pas_de_return p.main in 
  let type_context = extract_context p in
  typecheck_instruction type_context p.main;
  let liste_fonction = Symb_Tbl.bindings p.functions in typecheck_all_decl_function liste_fonction type_context;
  


